﻿using Autofac;
using AutoUpdaterDotNET;
using System.Globalization;
using System.Reflection;
using System.Windows.Threading;
using WinformDevFramework;
using WinformDevFramework.Core.Configuration;
using WinformDevFramework.IServices;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Services;

namespace WinformDevFarme
{
    public partial class FrmLogin : Form
    {
        private readonly ISysUserServices _sysUserServices;
        private IsysUserRoleServices _sysUserRoleServices;
        private IsysRoleMenuServices _sysRoleMenuServices;
        private ISysMenuServices _sysMenuServices;

        public FrmLogin(ISysUserServices sysUserServices,IsysUserRoleServices sysUserRoleServices, IsysRoleMenuServices sysRoleMenuServices, ISysMenuServices sysMenuServices)
        {
            _sysUserServices = sysUserServices;
            _sysUserRoleServices = sysUserRoleServices;
            _sysRoleMenuServices = sysRoleMenuServices;
            _sysMenuServices = sysMenuServices;
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAccount.Text))
            {
                lblTip.Text = "账号不能为空！";
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                lblTip.Text = "密码不能为空！";
                return;
            }
            var user=_sysUserServices.QueryListByClause(p=>p.Username.Equals(txtAccount.Text)).FirstOrDefault();
            if (user == null)
            {
                lblTip.Text = "用户不存在！";
                return;
            }

            if (!txtPassword.Text.Equals(user.Password))
            {
                lblTip.Text = "密码错误！";
                return;
            }
            //修改最后登录时间
            user.LastLoginTime = DateTime.Now;
            _sysUserServices.Update(user);
            lblTip.Text = string.Empty;
            AppInfo.User = user;
            //获取功能权限
            var roleIDs = _sysUserRoleServices.QueryListByClause(p => p.UserID == user.ID).Select(p=>p.RoleID).Select(x=>x??0).ToList();
            ISet<int> menuIds=new HashSet<int>();
            foreach (var roleID in roleIDs)
            {
                _sysRoleMenuServices.QueryListByClause(p => p.RoleID == roleID)
                    .ForEach(p => menuIds.Add(p.MenuID ?? 0));
            }

            AppInfo.UserMenus = _sysMenuServices.QueryByIDs(menuIds.ToArray());
            var fm = AppInfo.Container.ResolveNamed<Form>("FrmMain");
            this.Hide();
            fm.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            if (AppSettingsConstVars.AtuoUpdate)
            {
                AutoUpdater.CheckForUpdateEvent += AutoUpdater_CheckForUpdateEvent;
                AutoUpdater.ApplicationExitEvent += AutoUpdater_ApplicationExitEvent;
                AutoUpdater.Start(AppSettingsConstVars.Url);
            }
            
        }

        private void AutoUpdater_ApplicationExitEvent()
        {
            
        }

        private void AutoUpdater_CheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args != null&&new Version(args.CurrentVersion).CompareTo(new Version(AppSettingsConstVars.Version))>0)
            {
                FrmUpdate frm=new FrmUpdate();
                frm.url = args.ChangelogURL;
                frm.newVsersion = args.CurrentVersion;
                frm.oldVersion = AppSettingsConstVars.Version;
                var dialogResult = frm.ShowDialog();
                if (dialogResult == DialogResult.Yes)
                {
                    if (AutoUpdater.DownloadUpdate(args))
                    {
                        AppSettingsHelper.SetContent(args.CurrentVersion, "Update", "Version");
                        Application.Exit();
                    }
                }
            }
        }
    }
}
