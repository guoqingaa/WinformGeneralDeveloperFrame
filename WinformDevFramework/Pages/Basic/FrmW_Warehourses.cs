using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;

namespace WinformDevFramework
{
    public partial class Frmw_Warehouse : BaseForm1
    {
        private Iw_Warehouseervices _w_Warehouseervices;
        private ISysUserServices  _sysUserServices;
        public Frmw_Warehouse(Iw_Warehouseervices w_Warehouseervices, ISysUserServices sysUserServices)
        {
            _w_Warehouseervices= w_Warehouseervices;
            _sysUserServices= sysUserServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = _w_Warehouseervices.Query();

            this.comboBoxPerson.DataSource = _sysUserServices.Query();
            comboBoxPerson.ValueMember = "ID";
            comboBoxPerson.DisplayMember = "Fullname";
            // 设置隐藏列
            this.dataGridViewList.Columns["WarehouseName"].HeaderText = "仓库名称";
            this.dataGridViewList.Columns["Address"].HeaderText = "地址";
            this.dataGridViewList.Columns["StorageFee"].HeaderText = "仓储费";
            this.dataGridViewList.Columns["WarehouseCode"].HeaderText = "仓库编码";
            this.dataGridViewList.Columns["ChargePersonID"].Visible = false;
            this.dataGridViewList.Columns["SortOrder"].HeaderText = "排序";
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";


            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }


        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_Warehouse;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            Address.Text=model.Address;
            WarehoursesCode.Text = model.WarehouseCode;
            WarehouseName.Text = model.WarehouseName;
            StorageFee.Value = model.StorageFee ?? 0;
            comboBoxPerson.SelectedValue = model.ChargePersonID;
            SortOrder.Text = model.SortOrder.ToString();
            Remark.Text = model.Remark;
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            WarehoursesCode.ReadOnly = true;
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_Warehouse model = new w_Warehouse();
                    // TODO获取界面的数据
                    model.Address= Address.Text;
                    model.WarehouseName= WarehouseName.Text;
                    model.StorageFee = StorageFee.Value;
                    model.ChargePersonID= (int)comboBoxPerson.SelectedValue;
                    model.SortOrder= SortOrder.ToInt32();
                    model.Remark= Remark.Text;
                    model.WarehouseCode = WarehoursesCode.Text;
                    var id = _w_Warehouseervices.Insert(model);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                // 修改
                else
                {
                    w_Warehouse model = _w_Warehouseervices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.Address = Address.Text;
                    model.WarehouseName = WarehouseName.Text;
                    model.StorageFee = StorageFee.Value;
                    model.ChargePersonID = (int)comboBoxPerson.SelectedValue;
                    model.SortOrder = SortOrder.ToInt32();
                    model.Remark = Remark.Text;
                    model.WarehouseCode = WarehoursesCode.Text;
                    if (_w_Warehouseervices.Update(model))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = _w_Warehouseervices.Query();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _w_Warehouseervices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = _w_Warehouseervices.Query();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            if (string.IsNullOrEmpty(WarehouseName.Text))
            {
                MessageBox.Show("仓库名称不能为空！");
                return false;
            }
            return true;
        }
    }
}
