namespace WinformDevFramework
{
    partial class Frmw_Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPayCode = new System.Windows.Forms.Label();
            this.PayCode = new System.Windows.Forms.TextBox();
            this.lblSupplierCode = new System.Windows.Forms.Label();
            this.SupplierCode = new System.Windows.Forms.TextBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.lblPayDate = new System.Windows.Forms.Label();
            this.PayDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblSettlementCode = new System.Windows.Forms.Label();
            this.SettlementCode = new System.Windows.Forms.TextBox();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.SupplierName = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.BuyCode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PayCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SettlementName = new System.Windows.Forms.ComboBox();
            this.lblSettlementName = new System.Windows.Forms.Label();
            this.SettlementType = new System.Windows.Forms.ComboBox();
            this.lblSettlementType = new System.Windows.Forms.Label();
            this.MakeUserID = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Controls.Add(this.tabControl2);
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Controls.SetChildIndex(this.groupBox1, 0);
            this.tabDataEdit.Controls.SetChildIndex(this.tabControl2, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUserID);
            this.groupBox1.Controls.Add(this.SettlementType);
            this.groupBox1.Controls.Add(this.lblSettlementType);
            this.groupBox1.Controls.Add(this.SettlementName);
            this.groupBox1.Controls.Add(this.lblSettlementName);
            this.groupBox1.Controls.Add(this.SupplierName);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.lblTotalPrice);
            this.groupBox1.Controls.Add(this.SettlementCode);
            this.groupBox1.Controls.Add(this.lblSettlementCode);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.PayDate);
            this.groupBox1.Controls.Add(this.lblPayDate);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Controls.Add(this.SupplierCode);
            this.groupBox1.Controls.Add(this.lblSupplierCode);
            this.groupBox1.Controls.Add(this.PayCode);
            this.groupBox1.Controls.Add(this.lblPayCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblPayCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.PayCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblPayDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.PayDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementType, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementType, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUserID, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Size = new System.Drawing.Size(53, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(36, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(70, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSave.Size = new System.Drawing.Size(53, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(104, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCanel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(138, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(172, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetPW.Size = new System.Drawing.Size(43, 14);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(253, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClose.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(219, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearch.Size = new System.Drawing.Size(53, 23);
            // 
            // lblPayCode
            // 
            this.lblPayCode.Location = new System.Drawing.Point(10, 10);
            this.lblPayCode.Name = "lblPayCode";
            this.lblPayCode.Size = new System.Drawing.Size(85, 23);
            this.lblPayCode.TabIndex = 2;
            this.lblPayCode.Text = "付款单号";
            this.lblPayCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PayCode
            // 
            this.PayCode.Location = new System.Drawing.Point(100, 10);
            this.PayCode.Name = "PayCode";
            this.PayCode.PlaceholderText = "为空自动生成";
            this.PayCode.Size = new System.Drawing.Size(130, 23);
            this.PayCode.TabIndex = 1;
            // 
            // lblSupplierCode
            // 
            this.lblSupplierCode.Location = new System.Drawing.Point(450, 10);
            this.lblSupplierCode.Name = "lblSupplierCode";
            this.lblSupplierCode.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierCode.TabIndex = 4;
            this.lblSupplierCode.Text = "供应商编码";
            this.lblSupplierCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierCode
            // 
            this.SupplierCode.Location = new System.Drawing.Point(540, 10);
            this.SupplierCode.Name = "SupplierCode";
            this.SupplierCode.Size = new System.Drawing.Size(130, 23);
            this.SupplierCode.TabIndex = 3;
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.Location = new System.Drawing.Point(230, 10);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierName.TabIndex = 6;
            this.lblSupplierName.Text = "供应商名称";
            this.lblSupplierName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPayDate
            // 
            this.lblPayDate.Location = new System.Drawing.Point(10, 45);
            this.lblPayDate.Name = "lblPayDate";
            this.lblPayDate.Size = new System.Drawing.Size(85, 23);
            this.lblPayDate.TabIndex = 8;
            this.lblPayDate.Text = "付款时间";
            this.lblPayDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PayDate
            // 
            this.PayDate.Location = new System.Drawing.Point(100, 45);
            this.PayDate.Name = "PayDate";
            this.PayDate.Size = new System.Drawing.Size(130, 23);
            this.PayDate.TabIndex = 7;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(230, 45);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 10;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSettlementCode
            // 
            this.lblSettlementCode.Location = new System.Drawing.Point(230, 80);
            this.lblSettlementCode.Name = "lblSettlementCode";
            this.lblSettlementCode.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementCode.TabIndex = 14;
            this.lblSettlementCode.Text = "结算账户编码";
            this.lblSettlementCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SettlementCode
            // 
            this.SettlementCode.Location = new System.Drawing.Point(320, 80);
            this.SettlementCode.Name = "SettlementCode";
            this.SettlementCode.Size = new System.Drawing.Size(130, 23);
            this.SettlementCode.TabIndex = 13;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Location = new System.Drawing.Point(450, 45);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice.TabIndex = 18;
            this.lblTotalPrice.Text = "实付金额";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice
            // 
            this.TotalPrice.Location = new System.Drawing.Point(540, 45);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice.TabIndex = 17;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 115);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 20;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 115);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 19;
            // 
            // SupplierName
            // 
            this.SupplierName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SupplierName.FormattingEnabled = true;
            this.SupplierName.Location = new System.Drawing.Point(320, 10);
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Size = new System.Drawing.Size(130, 25);
            this.SupplierName.TabIndex = 21;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(3, 147);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1155, 414);
            this.tabControl2.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(1147, 384);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "结算明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuyCode,
            this.PayCodeD,
            this.PayDetailCode,
            this.Amount1,
            this.Amount2,
            this.Amount3,
            this.Amount4,
            this.RemarkD});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(1141, 378);
            this.dataGridViewDetail.TabIndex = 0;
            // 
            // BuyCode
            // 
            this.BuyCode.HeaderText = "采购单号";
            this.BuyCode.MinimumWidth = 9;
            this.BuyCode.Name = "BuyCode";
            this.BuyCode.Width = 62;
            // 
            // PayCodeD
            // 
            this.PayCodeD.DataPropertyName = "PayCode";
            this.PayCodeD.HeaderText = "付款单号";
            this.PayCodeD.MinimumWidth = 9;
            this.PayCodeD.Name = "PayCodeD";
            this.PayCodeD.ReadOnly = true;
            this.PayCodeD.Width = 81;
            // 
            // PayDetailCode
            // 
            this.PayDetailCode.DataPropertyName = "PayDetailCode";
            this.PayDetailCode.HeaderText = "付款明细单号";
            this.PayDetailCode.MinimumWidth = 9;
            this.PayDetailCode.Name = "PayDetailCode";
            this.PayDetailCode.ReadOnly = true;
            this.PayDetailCode.Width = 105;
            // 
            // Amount1
            // 
            this.Amount1.DataPropertyName = "Amount1";
            this.Amount1.HeaderText = "单据金额";
            this.Amount1.MinimumWidth = 9;
            this.Amount1.Name = "Amount1";
            this.Amount1.ReadOnly = true;
            this.Amount1.Width = 81;
            // 
            // Amount2
            // 
            this.Amount2.DataPropertyName = "Amount2";
            this.Amount2.HeaderText = "已结金额";
            this.Amount2.MinimumWidth = 9;
            this.Amount2.Name = "Amount2";
            this.Amount2.ReadOnly = true;
            this.Amount2.Width = 81;
            // 
            // Amount3
            // 
            this.Amount3.DataPropertyName = "Amount3";
            this.Amount3.HeaderText = "未结金额";
            this.Amount3.MinimumWidth = 9;
            this.Amount3.Name = "Amount3";
            this.Amount3.ReadOnly = true;
            this.Amount3.Width = 81;
            // 
            // Amount4
            // 
            this.Amount4.DataPropertyName = "Amount4";
            this.Amount4.HeaderText = "本次结算金额";
            this.Amount4.MinimumWidth = 9;
            this.Amount4.Name = "Amount4";
            this.Amount4.Width = 105;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // SettlementName
            // 
            this.SettlementName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementName.FormattingEnabled = true;
            this.SettlementName.Location = new System.Drawing.Point(100, 78);
            this.SettlementName.Name = "SettlementName";
            this.SettlementName.Size = new System.Drawing.Size(130, 25);
            this.SettlementName.TabIndex = 24;
            // 
            // lblSettlementName
            // 
            this.lblSettlementName.Location = new System.Drawing.Point(10, 80);
            this.lblSettlementName.Name = "lblSettlementName";
            this.lblSettlementName.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementName.TabIndex = 23;
            this.lblSettlementName.Text = "结算账户名称";
            this.lblSettlementName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SettlementType
            // 
            this.SettlementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementType.FormattingEnabled = true;
            this.SettlementType.Location = new System.Drawing.Point(540, 78);
            this.SettlementType.Name = "SettlementType";
            this.SettlementType.Size = new System.Drawing.Size(130, 25);
            this.SettlementType.TabIndex = 26;
            // 
            // lblSettlementType
            // 
            this.lblSettlementType.Location = new System.Drawing.Point(450, 80);
            this.lblSettlementType.Name = "lblSettlementType";
            this.lblSettlementType.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementType.TabIndex = 25;
            this.lblSettlementType.Text = "结算方式";
            this.lblSettlementType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MakeUserID
            // 
            this.MakeUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUserID.FormattingEnabled = true;
            this.MakeUserID.Location = new System.Drawing.Point(320, 46);
            this.MakeUserID.Name = "MakeUserID";
            this.MakeUserID.Size = new System.Drawing.Size(130, 25);
            this.MakeUserID.TabIndex = 33;
            // 
            // Frmw_Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_Payment";
            this.Text = "Frmw_Payment";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox PayCode;
        private Label lblPayCode;
        private TextBox SupplierCode;
        private Label lblSupplierCode;
        private Label lblSupplierName;
        private DateTimePicker PayDate;
        private Label lblPayDate;
        private Label lblMakeUserID;
        private TextBox SettlementCode;
        private Label lblSettlementCode;
        private TextBox TotalPrice;
        private Label lblTotalPrice;
        private TextBox Remark;
        private Label lblRemark;
        private ComboBox SupplierName;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewComboBoxColumn BuyCode;
        private DataGridViewTextBoxColumn PayCodeD;
        private DataGridViewTextBoxColumn PayDetailCode;
        private DataGridViewTextBoxColumn Amount1;
        private DataGridViewTextBoxColumn Amount2;
        private DataGridViewTextBoxColumn Amount3;
        private DataGridViewTextBoxColumn Amount4;
        private DataGridViewTextBoxColumn RemarkD;
        private ComboBox SettlementName;
        private Label lblSettlementName;
        private ComboBox SettlementType;
        private Label lblSettlementType;
        private ComboBox MakeUserID;
    }
}