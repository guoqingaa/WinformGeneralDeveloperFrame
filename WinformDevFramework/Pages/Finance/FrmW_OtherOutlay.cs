using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;

namespace WinformDevFramework
{
    public partial class Frmw_OtherOutlay : BaseForm1
    {
        private Iw_OtherOutlayServices _w_OtherOutlayServices;
        private Iw_CustomerServices _w_CustomerServices;
        private Iw_SupplierServices _w_SupplierServices;
        private Iw_SettlementAccountServices _w_SettlementAccountServices;
        private IsysDicDataServices _w_SysDicDataServices;
        private ISysUserServices _w_SysUserServices;
        public Frmw_OtherOutlay(Iw_OtherOutlayServices w_OtherOutlayServices, Iw_CustomerServices w_CustomerServices, Iw_SupplierServices w_SupplierServices, Iw_SettlementAccountServices w_SettlementAccountServices, IsysDicDataServices w_SysDicDataServices, ISysUserServices w_SysUserServices)
        {
            _w_OtherOutlayServices = w_OtherOutlayServices;
            _w_CustomerServices = w_CustomerServices;
            _w_SupplierServices = w_SupplierServices;
            _w_SettlementAccountServices = w_SettlementAccountServices;
            _w_SysDicDataServices = w_SysDicDataServices;
            _w_SysUserServices = w_SysUserServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["OtherOutlayCode"].HeaderText = "其他支出单号";
            this.dataGridViewList.Columns["FlatCode"].HeaderText = "对方单位编码";
            this.dataGridViewList.Columns["FlatName"].HeaderText = "对方单位名称";
            this.dataGridViewList.Columns["InvicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["SettlementCode"].HeaderText = "结算账户编码";
            this.dataGridViewList.Columns["SettlementName"].HeaderText = "结算账户名称";
            this.dataGridViewList.Columns["OutlayTypeID"].HeaderText = "收入类别ID";
            this.dataGridViewList.Columns["OutlayType"].HeaderText = "收入类别";
            this.dataGridViewList.Columns["TotalPrice"].HeaderText = "金额";
            this.dataGridViewList.Columns["MakeUserID"].Visible = false;
            this.dataGridViewList.Columns["ReviewUserID"].Visible = false;
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";

            //设置结算账户
            SettlementName.DataSource = _w_SettlementAccountServices.Query();
            SettlementName.DisplayMember = "Name";
            SettlementName.ValueMember = "Code";

            //设置对方单位名称

            //客户
            var customers = _w_CustomerServices.Query();
            //供应商
            var suppliers = _w_SupplierServices.Query();
            List<FlatInfo> flats = new List<FlatInfo>();
            customers.ForEach(p => flats.Add(new FlatInfo()
            {
                Name = p.CustomerName,
                Code = p.CustomerCode
            }));
            suppliers.ForEach(p => flats.Add(new FlatInfo()
            {
                Name = p.SupplierName,
                Code = p.SupplierCode
            }));
            FlatName.DataSource = flats;
            FlatName.DisplayMember = "Name";
            FlatName.ValueMember = "Code";

            //设置支出类别
            OutlayTypeID.DataSource = _w_SysDicDataServices.QueryListByClause(p => p.DicTypeID == 2005);
            OutlayTypeID.DisplayMember = "DicData";
            OutlayTypeID.ValueMember = "ID";

            //设置制单人
            MakeUserID.DataSource = _w_SysUserServices.Query();
            MakeUserID.DisplayMember = "Fullname";
            MakeUserID.ValueMember = "ID";

            //设置审核人
            ReviewUserID.DataSource = _w_SysUserServices.Query();
            ReviewUserID.DisplayMember = "Fullname";
            ReviewUserID.ValueMember = "ID";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_OtherOutlay;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.OtherOutlayCode.Text = model.OtherOutlayCode.ToString();
            this.FlatCode.Text = model.FlatCode.ToString();
            this.FlatName.SelectedValue = model.FlatCode.ToString();
            this.InvicesDate.Text = model.InvicesDate.ToString();
            this.SettlementCode.Text = model.SettlementCode.ToString();
            this.SettlementName.SelectedValue = model.SettlementCode.ToString();
            this.OutlayTypeID.SelectedValue = model.OutlayTypeID.ToString();
            this.TotalPrice.Text = model.TotalPrice.ToString();
            this.MakeUserID.SelectedValue = model.MakeUserID;
            this.ReviewUserID.SelectedValue = model.ReviewUserID;
            this.Status.Text = model.Status.ToString();
            this.Remark.Text = model.Remark.ToString();
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
            FlatCode.Text = FlatName.SelectedValue.ToString();
            SettlementCode.Text = SettlementName.SelectedValue.ToString();

            MakeUserID.SelectedValue = AppInfo.User.ID;
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_OtherOutlay model = new w_OtherOutlay();
                    // TODO获取界面的数据
                    if (string.IsNullOrEmpty(this.OtherOutlayCode.Text))
                    {
                        model.OtherOutlayCode = $"QTZC{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.OtherOutlayCode = this.OtherOutlayCode.Text;
                    }
                    model.FlatCode = this.FlatCode.Text;
                    model.FlatName = this.FlatName.Text;
                    model.InvicesDate = this.InvicesDate.Value;
                    model.SettlementCode = this.SettlementCode.Text;
                    model.SettlementName = this.SettlementName.Text;
                    model.OutlayTypeID = this.OutlayTypeID.SelectedValue.ToInt32();
                    model.OutlayType = this.OutlayTypeID.Text;
                    model.TotalPrice = this.TotalPrice.Text.ToDecimal();
                    model.MakeUserID = this.MakeUserID.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.SelectedValue.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;
                    var id = _w_OtherOutlayServices.Insert(model);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        var settlementAccount =
                            _w_SettlementAccountServices.QueryByClause(p => p.Code == model.SettlementCode);
                        if (settlementAccount != null)
                        {
                            settlementAccount.NowAmount -= model.TotalPrice;
                            _w_SettlementAccountServices.Update(settlementAccount);
                        }
                        MessageBox.Show("保存成功！", "提示");
                        OtherOutlayCode.Text = model.OtherOutlayCode;
                    }
                }
                // 修改
                else
                {
                    w_OtherOutlay model = _w_OtherOutlayServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.OtherOutlayCode = this.OtherOutlayCode.Text;
                    model.FlatCode = this.FlatCode.Text;
                    model.FlatName = this.FlatName.Text;
                    model.InvicesDate = this.InvicesDate.Value;
                    model.SettlementCode = this.SettlementCode.Text;
                    model.SettlementName = this.SettlementName.Text;
                    model.OutlayTypeID = this.OutlayTypeID.SelectedValue.ToInt32();
                    model.OutlayType = this.OutlayTypeID.Text;
                    model.TotalPrice = this.TotalPrice.Text.ToDecimal();
                    model.MakeUserID = this.MakeUserID.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.SelectedValue.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;
                    if (_w_OtherOutlayServices.Update(model))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;

        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _w_OtherOutlayServices.DeleteById(Int32.Parse(txtID.Text));
                var settlementAccount = _w_SettlementAccountServices.QueryByClause(p => p.Code == SettlementCode.Text);
                if (settlementAccount != null)
                {
                    settlementAccount.NowAmount += TotalPrice.Text.ToDecimal();
                }
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_OtherOutlay> GetData()
        {
            List<w_OtherOutlay> data = new List<w_OtherOutlay>();
            data = _w_OtherOutlayServices.Query();
            return data;
        }
    }
}
