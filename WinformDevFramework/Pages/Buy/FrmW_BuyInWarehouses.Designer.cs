namespace WinformDevFramework
{
    partial class Frmw_BuyInWarehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBuyCode = new System.Windows.Forms.Label();
            this.BuyCode = new System.Windows.Forms.TextBox();
            this.lblSupplierCode = new System.Windows.Forms.Label();
            this.SupplierCode = new System.Windows.Forms.TextBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.SupplierName = new System.Windows.Forms.TextBox();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblBuyInWarehouseCode = new System.Windows.Forms.Label();
            this.BuyInWarehouseCode = new System.Windows.Forms.TextBox();
            this.lblInvoicesImage = new System.Windows.Forms.Label();
            this.InvoicesImage = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyInWarehouseCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyInWarehouseDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Warehouse = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.InvoicesImage);
            this.groupBox1.Controls.Add(this.lblInvoicesImage);
            this.groupBox1.Controls.Add(this.BuyInWarehouseCode);
            this.groupBox1.Controls.Add(this.lblBuyInWarehouseCode);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.SupplierName);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Controls.Add(this.SupplierCode);
            this.groupBox1.Controls.Add(this.lblSupplierCode);
            this.groupBox1.Controls.Add(this.BuyCode);
            this.groupBox1.Controls.Add(this.lblBuyCode);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyInWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyInWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            // 
            // lblBuyCode
            // 
            this.lblBuyCode.Location = new System.Drawing.Point(10, 10);
            this.lblBuyCode.Name = "lblBuyCode";
            this.lblBuyCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyCode.TabIndex = 2;
            this.lblBuyCode.Text = "采购订单";
            this.lblBuyCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyCode
            // 
            this.BuyCode.Location = new System.Drawing.Point(100, 10);
            this.BuyCode.Name = "BuyCode";
            this.BuyCode.Size = new System.Drawing.Size(130, 23);
            this.BuyCode.TabIndex = 1;
            this.BuyCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BuyCode_KeyPress);
            // 
            // lblSupplierCode
            // 
            this.lblSupplierCode.Location = new System.Drawing.Point(230, 10);
            this.lblSupplierCode.Name = "lblSupplierCode";
            this.lblSupplierCode.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierCode.TabIndex = 4;
            this.lblSupplierCode.Text = "供应商编号";
            this.lblSupplierCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierCode
            // 
            this.SupplierCode.Location = new System.Drawing.Point(320, 10);
            this.SupplierCode.Name = "SupplierCode";
            this.SupplierCode.ReadOnly = true;
            this.SupplierCode.Size = new System.Drawing.Size(130, 23);
            this.SupplierCode.TabIndex = 3;
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.Location = new System.Drawing.Point(450, 10);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierName.TabIndex = 6;
            this.lblSupplierName.Text = "供应商名称";
            this.lblSupplierName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierName
            // 
            this.SupplierName.Location = new System.Drawing.Point(540, 10);
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            this.SupplierName.Size = new System.Drawing.Size(130, 23);
            this.SupplierName.TabIndex = 5;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(230, 45);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 8;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(320, 45);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 7;
            // 
            // lblBuyInWarehouseCode
            // 
            this.lblBuyInWarehouseCode.Location = new System.Drawing.Point(10, 45);
            this.lblBuyInWarehouseCode.Name = "lblBuyInWarehouseCode";
            this.lblBuyInWarehouseCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyInWarehouseCode.TabIndex = 10;
            this.lblBuyInWarehouseCode.Text = "入库单号";
            this.lblBuyInWarehouseCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyInWarehouseCode
            // 
            this.BuyInWarehouseCode.Location = new System.Drawing.Point(100, 45);
            this.BuyInWarehouseCode.Name = "BuyInWarehouseCode";
            this.BuyInWarehouseCode.PlaceholderText = "为空自动生成";
            this.BuyInWarehouseCode.Size = new System.Drawing.Size(130, 23);
            this.BuyInWarehouseCode.TabIndex = 9;
            // 
            // lblInvoicesImage
            // 
            this.lblInvoicesImage.Location = new System.Drawing.Point(450, 45);
            this.lblInvoicesImage.Name = "lblInvoicesImage";
            this.lblInvoicesImage.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesImage.TabIndex = 12;
            this.lblInvoicesImage.Text = "单据附件";
            this.lblInvoicesImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesImage.Visible = false;
            // 
            // InvoicesImage
            // 
            this.InvoicesImage.Location = new System.Drawing.Point(540, 45);
            this.InvoicesImage.Name = "InvoicesImage";
            this.InvoicesImage.Size = new System.Drawing.Size(130, 23);
            this.InvoicesImage.TabIndex = 11;
            this.InvoicesImage.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 80);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 14;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 80);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 13;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(230, 80);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 16;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(320, 80);
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 15;
            this.Status.Visible = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(0, 118);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(786, 265);
            this.tabControl2.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(778, 235);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "入库明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.BuyInWarehouseCodeD,
            this.BuyInWarehouseDetailCode,
            this.BuyCodeD,
            this.Warehouse,
            this.GoodsCode,
            this.GoodsName,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.RemarkD});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(772, 229);
            this.dataGridViewDetail.TabIndex = 1;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            this.ID.Width = 27;
            // 
            // BuyInWarehouseCodeD
            // 
            this.BuyInWarehouseCodeD.DataPropertyName = "BuyInWarehouseCode";
            this.BuyInWarehouseCodeD.HeaderText = "采购入库单号";
            this.BuyInWarehouseCodeD.Name = "BuyInWarehouseCodeD";
            this.BuyInWarehouseCodeD.ReadOnly = true;
            this.BuyInWarehouseCodeD.Visible = false;
            this.BuyInWarehouseCodeD.Width = 86;
            // 
            // BuyInWarehouseDetailCode
            // 
            this.BuyInWarehouseDetailCode.DataPropertyName = "BuyReturnDetailCode";
            this.BuyInWarehouseDetailCode.HeaderText = "采购入库明细单号";
            this.BuyInWarehouseDetailCode.Name = "BuyInWarehouseDetailCode";
            this.BuyInWarehouseDetailCode.ReadOnly = true;
            this.BuyInWarehouseDetailCode.Visible = false;
            this.BuyInWarehouseDetailCode.Width = 110;
            // 
            // BuyCodeD
            // 
            this.BuyCodeD.DataPropertyName = "BuyCode";
            this.BuyCodeD.HeaderText = "采购单号";
            this.BuyCodeD.Name = "BuyCodeD";
            this.BuyCodeD.ReadOnly = true;
            this.BuyCodeD.Visible = false;
            this.BuyCodeD.Width = 62;
            // 
            // Warehouse
            // 
            this.Warehouse.HeaderText = "仓库";
            this.Warehouse.Name = "Warehouse";
            this.Warehouse.Width = 39;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GoodsCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GoodsCode.Width = 62;
            // 
            // GoodsName
            // 
            this.GoodsName.DataPropertyName = "GoodsName";
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.ReadOnly = true;
            this.GoodsName.Width = 81;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 81;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 81;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.Name = "Number";
            this.Number.Width = 57;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // Frmw_BuyInWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Frmw_BuyInWarehouse";
            this.Text = "Frmw_BuyInWarehouse";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox BuyCode;
            private Label lblBuyCode;
            private TextBox SupplierCode;
            private Label lblSupplierCode;
            private TextBox SupplierName;
            private Label lblSupplierName;
            private DateTimePicker InvoicesDate;
            private Label lblInvoicesDate;
            private TextBox BuyInWarehouseCode;
            private Label lblBuyInWarehouseCode;
            private TextBox InvoicesImage;
            private Label lblInvoicesImage;
            private TextBox Remark;
            private Label lblRemark;
            private TextBox Status;
            private Label lblStatus;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn BuyInWarehouseCodeD;
        private DataGridViewTextBoxColumn BuyInWarehouseDetailCode;
        private DataGridViewTextBoxColumn BuyCodeD;
        private DataGridViewComboBoxColumn Warehouse;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn RemarkD;
    }
}