using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Services;
using WinformDevFramework.Services.System;

namespace WinformDevFramework
{
    public partial class Frmw_SaleOutWarehouse : BaseForm1
    {
        private Iw_SaleOutWarehouseServices _w_SaleOutWarehouseServices;
        private Iw_SaleOutWarehouseDetailServices _w_SaleOutWarehouseDetailServices;
        private Iw_SaleServices _wSaleServices;
        private Iw_SaleDetailServices _w_SaleDetailServices;
        private Iw_Warehouseervices _w_Warehouseervices;
        private ISysUserServices _w_SysUserServices;
        public Frmw_SaleOutWarehouse(Iw_SaleOutWarehouseServices w_SaleOutWarehouseServices, Iw_SaleOutWarehouseDetailServices w_SaleOutWarehouseDetailServices, Iw_SaleServices wSaleServices, Iw_SaleDetailServices w_SaleDetailServices, Iw_Warehouseervices w_Warehouseervices, ISysUserServices sysUserServices)
        {
            _w_SaleOutWarehouseServices = w_SaleOutWarehouseServices;
            InitializeComponent();
            _w_SaleOutWarehouseDetailServices = w_SaleOutWarehouseDetailServices;
            _wSaleServices = wSaleServices;
            _w_SaleDetailServices = w_SaleDetailServices;
            _w_Warehouseervices = w_Warehouseervices;
            _w_SysUserServices = sysUserServices;
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["SaleCode"].HeaderText = "销售订单";
            this.dataGridViewList.Columns["SaleOutWarehouseCode"].HeaderText = "销售出库单号";
            this.dataGridViewList.Columns["CustomerCode"].HeaderText = "客户编码";
            this.dataGridViewList.Columns["CustomerName"].HeaderText = "客户名称";
            this.dataGridViewList.Columns["InvicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["InvicesFile"].Visible = false;
            this.dataGridViewList.Columns["MakeUserID"].Visible = false;
            this.dataGridViewList.Columns["ReviewUserID"].Visible = false;
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";

            //设置明细 仓库
            WarehouseName.DataSource = _w_Warehouseervices.Query();
            WarehouseName.DisplayMember = "WarehouseName";
            WarehouseName.ValueMember = "WarehouseCode";

            MakeUser.DataSource = _w_SysUserServices.Query();//值查找销售部门
            MakeUser.ValueMember = "ID";
            MakeUser.DisplayMember = "Fullname";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_SaleOutWarehouse;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.SaleCode.Text = model.SaleCode;
            this.SaleOutWarehouseCode.Text = model.SaleOutWarehouseCode;
            this.CustomerCode.Text = model.CustomerCode;
            this.CustomerName.Text = model.CustomerName;
            this.InvicesDate.Text = model.InvicesDate.ToString();
            this.InvicesFile.Text = model.InvicesFile;
            this.MakeUser.SelectedValue = model.MakeUserID;
            this.ReviewUserID.Text = model.ReviewUserID.ToString();
            this.Status.Text = model.Status;
            this.Remark.Text = model.Remark;
            SetDataToDetail(GetDetailData(model.SaleOutWarehouseCode));
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SetDataGridViewReadonlyStatus();
            dataGridViewDetail.Rows.Clear();
            MakeUser.SelectedValue = AppInfo.User.ID;
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SetDataGridViewReadonlyStatus();
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_SaleOutWarehouse model = new w_SaleOutWarehouse();
                    // TODO获取界面的数据
                    model.SaleCode = this.SaleCode.Text;
                    if (string.IsNullOrEmpty(this.SaleOutWarehouseCode.Text))
                    {
                        model.SaleOutWarehouseCode = $"XSCK{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.SaleOutWarehouseCode = this.SaleOutWarehouseCode.Text;
                    }
                    model.CustomerCode = this.CustomerCode.Text;
                    model.CustomerName = this.CustomerName.Text;
                    model.InvicesDate = this.InvicesDate.Value;
                    model.InvicesFile = this.InvicesFile.Text;
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.Text.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;


                    List<w_SaleOutWarehouseDetail> dataDetails = new List<w_SaleOutWarehouseDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_SaleOutWarehouseDetail detail = new w_SaleOutWarehouseDetail();
                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            detail.WarehouseCode = row.Cells["WarehouseName"].Value.ToString();
                            detail.WarehouseName = row.Cells["WarehouseName"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.SaleCode = row.Cells["SaleCodeD"].FormattedValue.ToString();
                            detail.SaleDetailCode = row.Cells["SaleDetailCode"].FormattedValue.ToString();
                            detail.SaleOutWarehouseCode = row.Cells["SaleOutWarehouseCodeD"].FormattedValue.ToString();
                            detail.SaleOutWarehouseDetailCode = row.Cells["SaleOutWarehouseDetailCode"].FormattedValue.ToString();

                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    int id = _w_SaleOutWarehouseServices.AddSaleOutWarehouseInfo(model, dataDetails);
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                        txtID.Text = id.ToString();
                        SaleOutWarehouseCode.Text = model.SaleOutWarehouseCode;
                        SetDataToDetail(GetDetailData(model.SaleOutWarehouseCode));
                    }
                }
                // 修改
                else
                {
                    w_SaleOutWarehouse model = _w_SaleOutWarehouseServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.SaleCode = this.SaleCode.Text;
                    model.SaleOutWarehouseCode = this.SaleOutWarehouseCode.Text;
                    model.CustomerCode = this.CustomerCode.Text;
                    model.CustomerName = this.CustomerName.Text;
                    model.InvicesDate = this.InvicesDate.Value;
                    model.InvicesFile = this.InvicesFile.Text;
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.Text.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;

                    List<w_SaleOutWarehouseDetail> dataDetails = new List<w_SaleOutWarehouseDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_SaleOutWarehouseDetail detail = new w_SaleOutWarehouseDetail();
                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            detail.WarehouseCode = row.Cells["WarehouseName"].Value.ToString();
                            detail.WarehouseName = row.Cells["WarehouseName"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.SaleCode = row.Cells["SaleCodeD"].FormattedValue.ToString();
                            detail.SaleDetailCode = row.Cells["SaleDetailCode"].FormattedValue.ToString();
                            detail.SaleOutWarehouseCode = row.Cells["SaleOutWarehouseCodeD"].FormattedValue.ToString();
                            detail.SaleOutWarehouseDetailCode = row.Cells["SaleOutWarehouseDetailCode"].FormattedValue.ToString();

                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    if (_w_SaleOutWarehouseServices.UpdateSaleOutWarehouseInfo(model, dataDetails))
                    {
                        MessageBox.Show("保存成功！", "提示");
                        SetDataToDetail(GetDetailData(model.SaleOutWarehouseCode));
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;

        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                if (_w_SaleOutWarehouseServices.DeleteSaleOutWarehouseInfo(new w_SaleOutWarehouse()
                {
                    ID = txtID.Text.ToInt32(),
                    SaleOutWarehouseCode = SaleOutWarehouseCode.Text

                }))
                {
                    formStatus = FormStatus.Del;
                    SetToolButtonStatus(formStatus);
                    //列表重新赋值
                    dataGridViewDetail.Rows.Clear();
                    this.dataGridViewList.DataSource = GetData();
                }

            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_SaleOutWarehouse> GetData()
        {
            List<w_SaleOutWarehouse> data = new List<w_SaleOutWarehouse>();
            data = _w_SaleOutWarehouseServices.Query();
            return data;
        }

        /// <summary>
        /// 明细表 设置数据 --主表带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_SaleOutWarehouseDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["SaleCodeD"].Value = p.SaleCode;
                row.Cells["GoodsName"].Value = p.GoodsName;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["Number"].Value = p.Number;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["WarehouseName"].Value = p.WarehouseCode;
                row.Cells["WarehouseCode"].Value = p.WarehouseCode;
                row.Cells["SaleDetailCode"].Value = p.SaleDetailCode;
                row.Cells["SaleOutWarehouseCodeD"].Value = p.SaleOutWarehouseCode;
                row.Cells["SaleOutWarehouseDetailCode"].Value = p.SaleOutWarehouseDetailCode;
            });
        }

        /// <summary>
        /// 明细表 设置数据 --销售单号带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_SaleDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["SaleCodeD"].Value = p.SaleCode;
                row.Cells["GoodsName"].Value = p.GoodsName;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["Number"].Value = 0;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["WarehouseName"].Value = "CK001";
                row.Cells["WarehouseCode"].Value = "CK001";
                row.Cells["SaleDetailCode"].Value = p.SaleDetailCode;
            });
        }
        /// <summary>
        /// 获取明细数据
        /// </summary>
        /// <returns></returns>
        private List<w_SaleOutWarehouseDetail> GetDetailData(string code)
        {
            List<w_SaleOutWarehouseDetail> data = new List<w_SaleOutWarehouseDetail>();
            data = _w_SaleOutWarehouseDetailServices.QueryListByClause(p => p.SaleOutWarehouseCode == code);
            return data;
        }

        private void SaleCode_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void SaleCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!string.IsNullOrEmpty(SaleCode.Text.Trim()))
                {
                    var data = _wSaleServices.QueryByClause(p => p.SaleCode.Equals(SaleCode.Text.Trim()));
                    if (data != null)
                    {
                        CustomerName.Text = data.CustomerName;
                        CustomerCode.Text = data.CustomerCode;

                        List<w_SaleDetail> details = new List<w_SaleDetail>();
                        details = _w_SaleDetailServices.QueryListByClause(p => p.SaleCode == data.SaleCode);
                        SetDataToDetail(details);
                    }
                    else
                    {
                        //GoodsName.DataSource=new List<w_SaleReturnDetail>();;
                        MessageBox.Show("销售单号不存在！");
                    }
                }
            }
        }
        private void SetDataGridViewReadonlyStatus()
        {
            dataGridViewDetail.ReadOnly = false;
            string[] cols = { "Number", "Warehouse", "RemarkD" };
            foreach (DataGridViewColumn column in dataGridViewDetail.Columns)
            {
                if (!cols.Contains(column.Name))
                {
                    column.ReadOnly = true;
                }
            }
        }
    }
}
