﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using WinformDevFramework.Models;

namespace WinformDevFramework
{
    public class AppInfo
    {
        public static IContainer Container { get; set; }
        public static sysUser User { get; set; }
        public static List<sysMenu> Menus { get; set; }

        /// <summary>
        /// 用户功能
        /// </summary>
        public static List<sysMenu> UserMenus { get; set; }
    }
}
