using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleReturnServices : BaseServices<w_SaleReturn>, Iw_SaleReturnServices
    {
        private readonly Iw_SaleReturnRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleReturnServices(IUnitOfWork unitOfWork, Iw_SaleReturnRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddSaleReturnInfo(w_SaleReturn buy, List<w_SaleReturnDetail> detail)
        {
            return _dal.AddSaleReturnInfo(buy, detail);
        }

        public bool UpdateSaleReturnInfo(w_SaleReturn buy, List<w_SaleReturnDetail> detail)
        {
            return _dal.UpdateSaleReturnInfo(buy, detail);
        }
    }
}