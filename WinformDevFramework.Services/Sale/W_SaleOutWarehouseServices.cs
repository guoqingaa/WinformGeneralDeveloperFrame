using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleOutWarehouseServices : BaseServices<w_SaleOutWarehouse>, Iw_SaleOutWarehouseServices
    {
        private readonly Iw_SaleOutWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleOutWarehouseServices(IUnitOfWork unitOfWork, Iw_SaleOutWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddSaleOutWarehouseInfo(w_SaleOutWarehouse buy, List<w_SaleOutWarehouseDetail> detail)
        {
            return _dal.AddSaleOutWarehouseInfo(buy, detail);
        }

        public bool UpdateSaleOutWarehouseInfo(w_SaleOutWarehouse buy, List<w_SaleOutWarehouseDetail> detail)
        {
            return _dal.UpdateSaleOutWarehouseInfo(buy, detail);

        }

        public bool DeleteSaleOutWarehouseInfo(w_SaleOutWarehouse buy)
        {
            return _dal.DeleteSaleOutWarehouseInfo(buy);
        }
    }
}