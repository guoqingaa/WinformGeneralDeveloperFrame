using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SupplierServices : BaseServices<w_Supplier>, Iw_SupplierServices
    {
        private readonly Iw_SupplierRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SupplierServices(IUnitOfWork unitOfWork, Iw_SupplierRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}