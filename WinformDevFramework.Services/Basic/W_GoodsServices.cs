using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class W_GoodsServices : BaseServices<W_Goods>, IW_GoodsServices
    {
        private readonly IW_GoodsRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public W_GoodsServices(IUnitOfWork unitOfWork, IW_GoodsRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}