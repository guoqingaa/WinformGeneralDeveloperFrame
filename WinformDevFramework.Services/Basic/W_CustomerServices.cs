using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_CustomerServices : BaseServices<w_Customer>, Iw_CustomerServices
    {
        private readonly Iw_CustomerRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_CustomerServices(IUnitOfWork unitOfWork, Iw_CustomerRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}