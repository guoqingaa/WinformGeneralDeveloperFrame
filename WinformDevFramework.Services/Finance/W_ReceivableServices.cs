using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_ReceivableServices : BaseServices<w_Receivable>, Iw_ReceivableServices
    {
        private readonly Iw_ReceivableRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_ReceivableServices(IUnitOfWork unitOfWork, Iw_ReceivableRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail)
        {
            return _dal.AddReceivableInfo(buy, detail);
        }

        public bool UpdateReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail)
        {
            return _dal.UpdateReceivableInfo(buy, detail);
        }

        public bool DeleteReceivableInfo(w_Receivable buy)
        {
            return _dal.DeleteReceivableInfo(buy);
        }
    }
}