using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyReturnDetailServices : BaseServices<w_BuyReturnDetail>, Iw_BuyReturnDetailServices
    {
        private readonly Iw_BuyReturnDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyReturnDetailServices(IUnitOfWork unitOfWork, Iw_BuyReturnDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}