using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyDetailServices : BaseServices<w_BuyDetail>, Iw_BuyDetailServices
    {
        private readonly Iw_BuyDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyDetailServices(IUnitOfWork unitOfWork, Iw_BuyDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}