using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyInWarehouseDetailServices : BaseServices<w_BuyInWarehouseDetail>, Iw_BuyInWarehouseDetailServices
    {
        private readonly Iw_BuyInWarehouseDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyInWarehouseDetailServices(IUnitOfWork unitOfWork, Iw_BuyInWarehouseDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}