using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyReturnServices : BaseServices<w_BuyReturn>, Iw_BuyReturnServices
    {
        private readonly Iw_BuyReturnRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyReturnServices(IUnitOfWork unitOfWork, Iw_BuyReturnRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddBuyReturnInfo(w_BuyReturn buy, List<w_BuyReturnDetail> detail)
        {
            return _dal.AddBuyReturnInfo(buy, detail);
        }

        public bool UpdateBuyReturnInfo(w_BuyReturn buy, List<w_BuyReturnDetail> detail)
        {
            return _dal.UpdateBuyReturnInfo(buy, detail);
        }
    }
}