using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyInWarehouseServices : BaseServices<w_BuyInWarehouse>, Iw_BuyInWarehouseServices
    {
        private readonly Iw_BuyInWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyInWarehouseServices(IUnitOfWork unitOfWork, Iw_BuyInWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddBuyInWarehouseInfo(w_BuyInWarehouse buy, List<w_BuyInWarehouseDetail> detail)
        {
            return _dal.AddBuyInWarehouseInfo(buy, detail);
        }

        public bool UpdateBuyInWarehouseInfo(w_BuyInWarehouse buy, List<w_BuyInWarehouseDetail> detail)
        {
            return _dal.UpdateBuyInWarehouseInfo(buy, detail);
        }

        public bool DeleteBuyInWarehouseInfo(w_BuyInWarehouse buy)
        {
            return _dal.DeleteBuyInWarehouseInfo(buy);
        }
    }
}