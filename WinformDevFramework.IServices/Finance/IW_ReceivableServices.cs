using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.Models;


namespace WinformDevFramework.IServices
{
    public interface Iw_ReceivableServices: IBaseServices<w_Receivable>
    {
        int AddReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail);
        bool UpdateReceivableInfo(w_Receivable buy, List<w_ReceivableDetail> detail);
        bool DeleteReceivableInfo(w_Receivable buy);

    }
}