﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_Receivable")]
    public partial class w_Receivable
    {
           public w_Receivable(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:收款单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceivableCode {get;set;}

           /// <summary>
           /// Desc:客户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerCode {get;set;}

           /// <summary>
           /// Desc:客户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerName {get;set;}

           /// <summary>
           /// Desc:收款日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReceivableDate {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

           /// <summary>
           /// Desc:结算账户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementCode {get;set;}

           /// <summary>
           /// Desc:结算账户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementName {get;set;}

           /// <summary>
           /// Desc:结算方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementType {get;set;}

           /// <summary>
           /// Desc:实收金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
