﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_SaleReturnDetail")]
    public partial class w_SaleReturnDetail
    {
           public w_SaleReturnDetail(){


           }
           /// <summary>
           /// Desc:销售退货单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleReturnCode {get;set;}

           /// <summary>
           /// Desc:销售退货明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleReturnDetailCode {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Number {get;set;}

           /// <summary>
           /// Desc:退货单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? UnitPrice {get;set;}

           /// <summary>
           /// Desc:折扣金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DiscountPrice {get;set;}

           /// <summary>
           /// Desc:退货金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:税率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxRate {get;set;}

           /// <summary>
           /// Desc:含税单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxUnitPrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxPrice {get;set;}

           /// <summary>
           /// Desc:含税金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalTaxPrice {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:销售单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode {get;set;}

    }
}
