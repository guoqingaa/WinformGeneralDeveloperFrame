﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_OtherIncome")]
    public partial class w_OtherIncome
    {
           public w_OtherIncome(){


           }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:其他收入单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OtherIncomeCode {get;set;}

           /// <summary>
           /// Desc:对方单位编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FlatCode {get;set;}

           /// <summary>
           /// Desc:对方单位名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FlatName {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvicesDate {get;set;}

           /// <summary>
           /// Desc:结算账户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementCode {get;set;}

           /// <summary>
           /// Desc:结算账户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementName {get;set;}

           /// <summary>
           /// Desc:收入类别ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IncomeTypeID {get;set;}

           /// <summary>
           /// Desc:收入类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IncomeType {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ReviewUserID {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
