﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("W_Goods")]
    public partial class W_Goods
    {
           public W_Goods(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:规格型号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:商品类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsType {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:进货价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RestockingPrice {get;set;}

           /// <summary>
           /// Desc:零售价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RetailPrice {get;set;}

           /// <summary>
           /// Desc:批发价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? WholesalePrice {get;set;}

           /// <summary>
           /// Desc:安全库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SafetyStock {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:商品图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsImages {get;set;}

           /// <summary>
           /// Desc:库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Stock {get;set;}

    }
}
