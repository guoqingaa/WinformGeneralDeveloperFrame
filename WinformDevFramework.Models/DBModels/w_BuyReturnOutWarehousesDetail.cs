﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_BuyReturnOutWarehouseDetail")]
    public partial class w_BuyReturnOutWarehouseDetail
    {
           public w_BuyReturnOutWarehouseDetail(){


           }
           /// <summary>
           /// Desc:采购退货单
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnCode {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:采购退货出库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnOutWarehouseCode {get;set;}

           /// <summary>
           /// Desc:采购退货出库明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnOutWarehouseDetailCode {get;set;}

           /// <summary>
           /// Desc:采购明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyDetailCode {get;set;}

           /// <summary>
           /// Desc:仓库编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseCode {get;set;}

           /// <summary>
           /// Desc:仓库名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WarehouseName {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Number {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
