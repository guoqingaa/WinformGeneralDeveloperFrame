﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_Sale")]
    public partial class w_Sale
    {
           public w_Sale(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:客户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerCode {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvoicesDate {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

           /// <summary>
           /// Desc:交货日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? DeliveryDate {get;set;}

           /// <summary>
           /// Desc:单据编号 SALE20230904212510  003
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode {get;set;}

           /// <summary>
           /// Desc:单据附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoicesFile {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:已结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice1 {get;set;}

           /// <summary>
           /// Desc:未结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice2 {get;set;}

           /// <summary>
           /// Desc:客户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerName {get;set;}

    }
}
