using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

using SqlSugar.Extensions;
using System.Collections;

namespace WinformDevFramework.Repository
{
    public class w_BuyInWarehouseRepository : BaseRepository<w_BuyInWarehouse>, Iw_BuyInWarehouseRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_BuyInWarehouseRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient=sqlSugar;
        }

        public int AddBuyInWarehouseInfo(w_BuyInWarehouse buy, List<w_BuyInWarehouseDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                //保存主表
                id = base.Insert(buy);
                int num = 1;
                detail.ForEach(p =>
                {
                    p.BuyInWarehouseCode = buy.BuyInWarehouseCode;
                    p.BuyCode = buy.BuyCode;
                    p.BuyInWarehouseDetailCode = buy.BuyInWarehouseCode + num.ToString("000");
                    num++;
                });
                //保存明细表
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();

                detail.ForEach(
                    p =>
                    {
                        var goods = _sqlSugarClient.Queryable<W_Goods>().First(m => m.GoodsCode == p.GoodsCode);
                        goods.Stock ??= 0;
                        goods.Stock += p.Number;
                        // 修改商品库存
                        _sqlSugarClient.Updateable<W_Goods>(goods).ExecuteCommand();
                    });
                
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        public bool UpdateBuyInWarehouseInfo(w_BuyInWarehouse buy, List<w_BuyInWarehouseDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buy);
                //删除明细数据
                DbBaseClient.Deleteable<w_BuyInWarehouseDetail>(p => p.BuyInWarehouseCode == buy.BuyInWarehouseCode).ExecuteCommand();
                var d = detail.Where(p => !string.IsNullOrEmpty(p.BuyInWarehouseDetailCode)).Select(p => p.BuyInWarehouseDetailCode).ToList();
                int num = 0;
                d.ForEach(p =>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.BuyInWarehouseCode))
                    {
                        num++;
                        p.BuyInWarehouseCode = buy.BuyInWarehouseCode;
                        p.BuyCode = buy.BuyCode;
                        p.BuyInWarehouseDetailCode = buy.BuyInWarehouseCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();

                detail.ForEach(p =>
                {
                    //查找该商品所有入库明细 统计库存
                    //采购入库
                    var cgrk = _sqlSugarClient.Queryable<w_BuyInWarehouseDetail>()
                        .Where(x => x.GoodsCode == p.GoodsCode).Sum(x => x.Number)??0;

                    //采购退货出库
                    var cgthuk = _sqlSugarClient.Queryable<w_BuyReturnOutWarehouseDetail>()
                        .Where(x => x.GoodsCode == p.GoodsCode).Sum(x => x.Number)??0;

                    //销售出库
                    var xsrk = _sqlSugarClient.Queryable<w_SaleOutWarehouseDetail>()
                        .Where(x => x.GoodsCode == p.GoodsCode).Sum(x => x.Number)??0;

                    //销售退货入库
                    var xsthrk= _sqlSugarClient.Queryable<w_SaleReturnInWarehouseDetail>()
                        .Where(x => x.GoodsCode == p.GoodsCode).Sum(x => x.Number)??0;

                    var goods = _sqlSugarClient.Queryable<W_Goods>().First(m => m.GoodsCode == p.GoodsCode);
                    goods.Stock = cgrk+ xsthrk- xsrk- cgthuk;
                    // 修改商品库存
                    _sqlSugarClient.Updateable<W_Goods>(goods).ExecuteCommand();
                });
                
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }

        public bool DeleteBuyInWarehouseInfo(w_BuyInWarehouse buy)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.DeleteById(buy.ID);
                var detail = DbBaseClient.Queryable<w_BuyInWarehouseDetail>()
                    .Where(p => p.BuyInWarehouseCode == buy.BuyInWarehouseCode).ToList();
                //删除明细数据
                DbBaseClient.Deleteable<w_BuyInWarehouseDetail>(p => p.BuyInWarehouseCode == buy.BuyInWarehouseCode).ExecuteCommand();
                 
                detail.ForEach(p =>
                {
                    var goods = _sqlSugarClient.Queryable<W_Goods>().First(m => m.GoodsCode == p.GoodsCode);
                    goods.Stock -= p.Number;
                    // 修改商品库存
                    _sqlSugarClient.Updateable<W_Goods>(goods).ExecuteCommand();
                });

                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}