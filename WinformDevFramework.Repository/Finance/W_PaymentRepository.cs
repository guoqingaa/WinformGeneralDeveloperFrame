using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SqlSugar.Extensions;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
using System.Diagnostics;

namespace WinformDevFramework.Repository
{
    public class w_PaymentRepository : BaseRepository<w_Payment>, Iw_PaymentRepository
    {
        private ISqlSugarClient _sqlSugarClient;
        public w_PaymentRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {
            _sqlSugarClient=sqlSugar;
        }

        public int AddPaymentInfo(w_Payment buy, List<w_PaymentDetail> detail)
        {
            int id = 0;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                id = base.Insert(buy);
                int num = 1;

                //修改采购订单 金额信息
                detail.ForEach(p =>
                {
                    var buyInfo = _sqlSugarClient.Queryable<w_Buy>().Where(x => x.BuyCode == p.BuyCode).First();
                    if (buyInfo != null)
                    {
                        buyInfo.TotalPrice1 += p.Amount4;
                        buyInfo.TotalPrice2 -= p.Amount4;
                        _sqlSugarClient.Updateable<w_Buy>(buyInfo).ExecuteCommand();
                    }
                });
                decimal? price = 0;
                detail.ForEach(p =>
                {
                    p.PayCode = buy.PayCode;
                    p.PayDetailCode = buy.PayCode + num.ToString("000");
                    p.Amount2 += p.Amount4;
                    p.Amount3 -= p.Amount4;
                    price += p.Amount4;
                    //p.Amount4 = 0;
                    num++;
                });
                //查找结算账户
                var settlementInfo = _sqlSugarClient.Queryable<w_SettlementAccount>()
                    .Where(p => p.Code == buy.SettlementCode).First();
                if (settlementInfo != null)
                {
                    settlementInfo.NowAmount -= price;
                }
                _sqlSugarClient.Updateable(settlementInfo).ExecuteCommand();
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return id;
        }

        public bool UpdatePaymentInfo(w_Payment buy, List<w_PaymentDetail> detail)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.Update(buy);
                //删除明细数据
                DbBaseClient.Deleteable<w_PaymentDetail>(p => p.PayCode == buy.PayCode).ExecuteCommand();
                var d = detail.Where(p => !string.IsNullOrEmpty(p.PayDetailCode)).Select(p => p.PayDetailCode).ToList();
                int num = 0;
                d.ForEach(p =>
                {
                    var m = p.Substring(p.Length - 3, 3).ObjToInt();
                    if (num < m)
                    {
                        num = m;
                    }
                });
                detail.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p.PayCode))
                    {
                        num++;
                        p.PayCode = buy.PayCode;
                        p.PayDetailCode = buy.PayCode + num.ToString("000");
                    }
                });
                //增加明细数据
                var r = _sqlSugarClient.Insertable(detail).ExecuteCommand();

                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }

        public bool DeletePaymentInfo(w_Payment buy)
        {
            bool result = false;
            try
            {
                _sqlSugarClient.Ado.BeginTran();
                result = base.DeleteById(buy.ID);
                var detail = _sqlSugarClient.Queryable<w_PaymentDetail>().Where(p => p.PayCode == buy.PayCode).ToList();
                //删除明细数据
                _sqlSugarClient.Deleteable<w_PaymentDetail>(p => p.PayCode == buy.PayCode).ExecuteCommand();
                decimal? price = 0;
                //修改采购订单数据
                detail.ForEach(p =>
                {
                    var buyInfo = _sqlSugarClient.Queryable<w_Buy>().Where(x => x.BuyCode == p.BuyCode).First();
                    if (buyInfo != null)
                    {
                        buyInfo.TotalPrice1 -= p.Amount4;
                        buyInfo.TotalPrice2 += p.Amount4;
                        price += p.Amount4;
                        _sqlSugarClient.Updateable(buyInfo).ExecuteCommand();
                    }
                });

                //查找结算账户
                var settlementInfo = _sqlSugarClient.Queryable<w_SettlementAccount>()
                    .Where(p => p.Code == buy.SettlementCode).First();
                if (settlementInfo != null)
                {
                    settlementInfo.NowAmount += price;
                    _sqlSugarClient.Updateable(settlementInfo).ExecuteCommand();
                }
                _sqlSugarClient.Ado.CommitTran();
            }
            catch (Exception e)
            {
                _sqlSugarClient.Ado.RollbackTran();
                throw;
            }
            return result;
        }
    }
}